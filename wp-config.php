<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'chisel_shavings' );

/** MySQL database username */
define( 'DB_USER', 'root' );

/** MySQL database password */
define( 'DB_PASSWORD', 'root' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'TL8ntLaeZKkjz+vgV=|U7#6B!i@s&YjVm_`M?qBfFJ*oR_VKYzs%=5(V<*U61%(x' );
define( 'SECURE_AUTH_KEY',  'r>q&3*]!OdA.z.GfpR/*TbF$<Cefg9@|qFEVSovRq.(}v<XS!D9e`s9JWK8qDgK.' );
define( 'LOGGED_IN_KEY',    'J^2blQ?@MlKlt SQQ`vBr@LxB<Z0Db9${=a=kv]TV%%Y@$6q8gya}#m@Wdt._5u/' );
define( 'NONCE_KEY',        '&`7M#DW@HezF,])H#uYfe]<eO%(|^D|/T`Y2_c+S#n DS!?8=a`8L]3HzqK8d}X<' );
define( 'AUTH_SALT',        '@Yzf-Rv$SeG(4rL&D/Tl|D/^G99qr:#[z>FrL<(?8I5;&2@1`K-<6PiQj<g(:fHf' );
define( 'SECURE_AUTH_SALT', '?loz3*g10^_vct=|Pd+Ux0Gc%+&WPk`Cu#sf(0D[Dg2CjR_a3,UXehhxkR(.]4F|' );
define( 'LOGGED_IN_SALT',   'mt=GTh,+|Cw%oW-n*Qy)JxwT:x5FPS8f5D4YJB*%P<lOCb@LM,<D!4HYFA~S~B<>' );
define( 'NONCE_SALT',       'Lcg2mG5.ylWkaN#`Sr^}Q|f(E`M<M4l~A3$SvG;e}{-*g{V&:7LW7t4K@;k0aWVb' );

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'wp_5ZYiTR_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define( 'WP_DEBUG', true );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', dirname( __FILE__ ) . '/' );
}

/** Sets up WordPress vars and included files. */
require_once( ABSPATH . 'wp-settings.php' );
