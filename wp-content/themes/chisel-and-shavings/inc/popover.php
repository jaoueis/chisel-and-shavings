<div class="popover-wrap" aria-label="popup window" aria-expanded="false">
    <div class="container">
        <section class="invite-details">
            <div class="cls-btn-wrap">
                <button aria-label="close"><span></span></button>
            </div>
            <header class="title-wrap">
                <h2>
                    <span>
                        <?php esc_html_e("You are Cordially Invited to the Launch", "chisel-and-shavings") ?>
                    </span>
                    <span>
                        <?php esc_html_e("of the", "chisel-and-shavings") ?>
                    </span>
                    <span>
                        <?php esc_html_e("New", "chisel-and-shavings") ?>
                    </span>
                    <span>
                        <?php esc_html_e("Company", "chisel-and-shavings") ?>
                    </span>
                    <span>
                        <?php esc_html_e("Website", "chisel-and-shavings") ?>
                    </span>
                </h2>
            </header>
            <div class="details-wrap">
                <?php
                if (get_field("lead_p")) {
                    echo "<p class='lead'>" . get_field("lead_p") . "</p>";
                }
                ?>
                <hr>
                <?php
                if (get_field("items_intro")) {
                    echo "<p class='items-intro'>" . get_field("items_intro") . "</p>";
                }
                ?>
                <?php
                if (get_field("items_content")) {
                    echo "<div class='details-main flex-center'>" . get_field("items_content") . "</div>";
                }
                ?>
            </div>
            <div class="form-wrap">
                <?php
                if (get_field("form_title")) {
                    echo "<h3>" . get_field("form_title") . "</h3>";
                }
                ?>
                <?php
                if (get_field("form_intro")) {
                    echo "<p>" . get_field("form_intro") . "</p>";
                }
                echo do_shortcode('[contact-form-7 id="20" title="RSVP"]');
                ?>
            </div>
        </section>
        <?php
        $img_right = get_field('image_right');

        if (!empty($img_right)): ?>
            <div class="img-right-wrap">
                <img src="<?php echo $img_right['url']; ?>" alt="<?php echo $img_right['alt']; ?>" class="rellax" data-rellax-speed="-4" />
            </div>
        <?php endif; ?>
    </div>
    <?php
    $img_left = get_field('image_left');

    if (!empty($img_left)): ?>
        <div class="img-left-wrap">
            <img src="<?php echo $img_left['url']; ?>" alt="<?php echo $img_left['alt']; ?>" class="rellax" data-rellax-speed="4" />
        </div>
    <?php endif; ?>
</div>