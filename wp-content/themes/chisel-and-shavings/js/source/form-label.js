(function () {
    var inputs = document.querySelectorAll('.wpcf7-text');

    for (var i = 0; i < inputs.length; i++) {
        inputs[i].addEventListener('focus', function () {
            this.parentNode.parentNode.classList.add('focus'); // double parentNode optimize for IE
        });

        inputs[i].addEventListener('blur', function () {
            if (this.value.length === 0) {
                this.parentNode.parentNode.classList.remove('focus'); // double parentNode optimize for IE
            }
        });
    }
})();