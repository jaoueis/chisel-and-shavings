(function () {
    var popup    = document.querySelector('.popover-wrap');
    var close    = document.querySelector('.cls-btn-wrap button');
    var isOpened = sessionStorage.getItem('popup-opened');

    document.addEventListener('DOMContentLoaded', function () {
        if (isOpened === null) {
            setTimeout(function () {
                sessionStorage.setItem('popup-opened', true);
                popup.setAttribute('aria-expanded', true);
                popup.setAttribute('style', 'visibility: visible; opacity: 1;'); // double setAttribute optimize for IE
            }, 3000);
        }
    }, false);

    close.addEventListener('click', function () {
        popup.setAttribute('aria-expanded', false);
        popup.setAttribute('style', 'visibility: hidden; opacity: 0;'); // double setAttribute optimize for IE
    });
})();