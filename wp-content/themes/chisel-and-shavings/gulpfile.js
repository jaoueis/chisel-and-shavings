'use strict';

// load plugins
const gulp         = require('gulp');
const gulpSass     = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const minifyCSS    = require('gulp-csso');
const concat       = require('gulp-concat');
const rename       = require('gulp-rename');
const terser       = require('gulp-terser');

// prevent errors stop Gulp tasks
function swallowError(error) {
    console.log(error.toString());
    this.emit('end');
}

// SASS task
function sass() {
    return gulp.src(['./sass/initial.scss', './sass/main.scss'])
               .pipe(gulpSass())
               .pipe(autoprefixer({
                   overrideBrowserslist: ['last 2 versions'],
                   cascade             : false
               }))
               .pipe(minifyCSS())
               .pipe(concat('style.css'))
               .on('error', swallowError)
               .pipe(gulp.dest('.'));
}

// transpile, concatenate and minify scripts
function js() {
    return gulp.src(['./js/source/libs/*.js', './js/source/*.js'])
               .pipe(concat('production.js'))
               .pipe(gulp.dest('./js/build/'))
               .pipe(rename('production.min.js'))
               .pipe(terser())
               .on('error', swallowError)
               .pipe(gulp.dest('./js/build/'));
}

// watch files
function watch() {
    gulp.watch(['./sass/*.scss', './sass/*/*.scss'], sass);
    gulp.watch(['./js/source/libs/*.js', './js/source/*.js'], js);
}

// tasks
gulp.task('sass', sass);
gulp.task('js', js);

// build
gulp.task('build', gulp.parallel('sass', 'js'));

// watch && run
gulp.task('default', gulp.series(watch, 'build'));